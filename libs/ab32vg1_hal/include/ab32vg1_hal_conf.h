/*
 * Copyright (c) 2020-2020, BLUETRUM Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef AB32VG1_HAL_CONF_H__
#define AB32VG1_HAL_CONF_H__

/*  System Configuration  */

/*  Includes  */
#include "ab32vg1_hal_gpio.h"
#include "ab32vg1_hal_uart.h"
#include "ab32vg1_hal_rcu.h"
#include "ab32vg1_hal_wdt.h"
#include "ab32vg1_hal_dac.h"
#include "ab32vg1_hal_sd.h"
#include "ab32vg1_hal_sdio.h"

#include <assert.h>

#endif
